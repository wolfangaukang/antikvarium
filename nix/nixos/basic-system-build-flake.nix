{
  description = "Basic flake to build systems and create a basic shell";
  inputs = {
    # In case we want to go full unstable
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    # In case we want to go unstable (but with tests)
    nixos.url = "github:nixos/nixpkgs/nixos-unstable";
    # In case we want to be on stable
    nixos-22.url = "github:nixos/nixpkgs/release-22.05";
    # For home-manager related operations
    home-manager = {
      url = "github:nix-community/home-manager";
      # Optional, in case we want home-manager to use the nixpkgs input
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Useful for hardware support with nixos
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    # Some other interesting inputs, where we will use the overlay
    nur.url = "github:nix-community/NUR";
    nixgl.url = "github:guibou/nixGL";
  };

  outputs = { self, nixpkgs, nixos, nixos-22, home-manager, nixos-hardware, nur, nixgl, ... }@inputs:
    let
      # We could grab this from nixpkgs, nixos or nixos-22 input.
      # It depends on how "stable" you want your system. Here we picked nixos-unstable.
      inherit (nixos.lib) nixosSystem;
      # Use this in case you want to generate home-manager configurations and build them
      # separately (ie. Non NixOS distro with home-manager)
      inherit (home-manager.lib) homeManagerConfiguration;
      # In case we want to use programs from other inputs, you need to add the overlay.
      # You must check if the project flake supports it:
      # NUR flake: https://github.com/nix-community/NUR/blob/3dc4cebb6d7f9a8658acb23fcec018f58b265b25/flake.nix#L5
      # nixGL flake: https://github.com/guibou/nixGL/blob/047a34b2f087e2e3f93d43df8e67ada40bf70e5c/flake.nix#L25
      overlays = [
        nixgl.overlay
        nur.overlay
      ];
      # Some variables I want to use for building my system
      username = "rurik";
      system = "x86_64-linux";
      # Needed for home-manager. Again, you could use the nixpkgs, nixos or nixos-22 input.
      pkgs = import nixpkgs { inherit system overlays; };
    in {
      # Here are our outputs that will be accessible on our flake.
      homeConfigurations = {
        # Check the configuration here https://nix-community.github.io/home-manager/index.html#sec-flakes-standalone
        nonnixos = homeManagerConfiguration {
          inherit pkgs;
          extraSpecialArgs = { inherit username; };
          modules = [
            ./path/to/home.nix
            ./another/file.nix
          ];
        };
      };
      nixosConfigurations = {
        mynixossystem = nixosSystem {
          inherit system;
          modules = [
            ./configuration.nix
            # If you want stuff from inputs
            nixos-hardware.nixosModules.lenovo-thinkpad-t430
            # Check the configuration here https://nix-community.github.io/home-manager/index.html#sec-flakes-nixos-module
            home-manager.nixosModules.home-manager {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                users."${username}" = import ./home.nix;
              };
              nixpkgs = {
                config.allowUnfree = true;
                overlays = overlays;
              };
            }
          ];
        };
      };
      # Easy setup for testing
      # Instead of using `home-manager switch --flake ".#homeConfigurations.nonnixos.activationPackage"`
      # With the below variable you will go to `home-manager switch --flake ".#nonnixos"`
      nonnixos = self.homeConfigurations.nonnixos.activationPackage;
      # Instead of using `nixos-rebuild switch --flake ".#nixosConfigurations.eyjafjallajokull.config.system.build.toplevel"`
      # With the below variable you will go to `nixos-rebuild switch --flake ".#mynixossystem"`
      mynixossystem = self.nixosConfigurations.mynixossystem.config.system.build.toplevel;
    };
}

