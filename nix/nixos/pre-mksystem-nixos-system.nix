{ username, overlays }:

inputs: {
  # Personal modules are already loaded by fup
  modules = [
    ./configuration.nix
    inputs.nixos-hardware.nixosModules.lenovo-thinkpad-t430
    inputs.home-manager.nixosModules.home-manager ( import ./hm-module.nix { inherit inputs overlays username; } )
  ];
}
