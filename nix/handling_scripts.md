# Creating Nix Flake app scripts

## 1. Importing script from program already packaged
```nix
apps."example" = {
	type = "app";
	program = "${example}/bin/example";
};
```

## 2. Importing script
```nix
apps."example" = {
	type = "app";
	program = toString (writeShellScript "example_script" (builtins.readFile ./path/to/script.sh));
};
```

## 3. Writing a script
```nix
apps."example" = {
	type = "app";
	program = toString (writeShellScript "example_script" ''
	#!/usr/bin/env bash

	echo "Hello"
	'');
};
```

## 4. Creating a package
```nix
# Using resholve as recommended way to write scripts
packages."${name}" = resholve.mkDerivation {
	pname = name;
	version = version;
	src = ./.;
	
	installPhase = ''
		install -D example-script.sh $out/bin/example-script
	'';
	
	solutions = {
		default = {
			scripts = [ "bin/example-script" ];
			interpreter = "${bash}/bin/bash";
			inputs = [ programX ];
		};
	};
};
# mkApp is specific from flake-utils
apps."example-script" = mkApp {
	drv = packages."${name}";
	exePath = "/bin/example-script";
};
```
